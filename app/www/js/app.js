angular.module('ionic-firebase-seed', ['ionic', 'firebase'])

// TODO: Replace this with your own Firebase URL: https://firebase.com/signup
.constant('FBURL', 'https://vivid-fire-6256.firebaseio.com/')

.factory('Auth', function($firebaseAuth, FBURL, $window) {
  var ref = new $window.Firebase(FBURL);
  return $firebaseAuth(ref);
})

.factory('Messages', function($firebaseArray, FBURL, $window) {
  var ref = new $window.Firebase(FBURL + '/messages');
  return $firebaseArray(ref);
})

.factory('Stories', function($firebaseObject, FBURL, $window) {
  var ref = new $window.Firebase(FBURL + '/stories');
  return $firebaseObject(ref);
})

.factory('Options', function($firebaseArray, FBURL, $window) {
  var ref = new $window.Firebase(FBURL + '/options');
  return $firebaseArray(ref);
})


.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
          url: '/',
          templateUrl: 'templates/main.html',
          controller: 'AppCtrl'
      })
      .state('stories', {
          url: '/stories/:story_id',
          templateUrl: 'templates/story_template.html',
          controller: 'StoryCtrl'
      })
      .state('option', {
          url: '/stories/:story_id/:option_id',
          templateUrl: 'templates/option_template.html',
          controller: 'OptionCtrl'
      })
      .state('new_option', {
          url: '/new_option',
          templateUrl: 'templates/new_option.html',
          controller: 'NewOptionCtrl'
      })
      .state('image_search', {
          url: '/image_search',
          templateUrl: 'templates/image_search.html',
          controller: 'ImageSearchCtrl'
      });
    $urlRouterProvider.otherwise('/');
})
.controller('NewOptionCtrl', function($scope, $rootScope, $state, $stateParams, Stories) {
  $scope.img_url = $rootScope.url;
  $rootScope.$watch('urlChoosen', function(newValue, oldValue) {
    if(newValue !== oldValue){
      $scope.img_url = newValue;
    }
  });
  $scope.story_id = $stateParams.story_id;
  $scope.addOption = function (new_option) {
    Stories.options.$add({
      title: $scope.title,
      body_text: $scope.body_text,
      image_url: $scope.img_url,
      choice_text: $scope.choice_text
    });
  };
})

.controller('ImageSearchCtrl', function($scope, $rootScope, $state, $stateParams, Stories) {
  $scope.query = null;
  $scope.searchImage = function (query) {
    $scope.getResults(query, function(err, data){
      if(err){
        console.log('search image error');
      }else {
        var thumbnails = data.map(function (imageObject){
          return imageObject.Thumbnail;
        });
        $scope.imageResults = thumbnails;
      }
    });
  };

  $scope.choosePhoto = function (url) {
    $rootScope.urlChoosen = url;
    $state.go('new_option');
  };
})

.controller('OptionCtrl', function($scope, $state, $stateParams, Stories, Options) {
  $scope.story_id = $stateParams.story_id;
  var watchHandler = function(valueChanged){
    var key = $stateParams.option_id;
    var indexOfOption = Options.findIndex(function(el){return el.$id === key;});
    if(indexOfOption > -1){
      $scope.option = Options[indexOfOption];
      var mappedOptions = [];
      for(var key in $scope.option.options){
        var indexOfOption = Options.findIndex(function(el){return el.$id === key;})
        if(indexOfOption > -1){
          mappedOptions.push(Options[indexOfOption])
        }
      }
      $scope.option.mappedOptions = mappedOptions;
    }
  };
  $scope.options = Options;

  if($scope.options.length > 0){
    $scope.previousOption = $scope.option;
    var key = $stateParams.option_id;
    var indexOfOption = Options.findIndex(function(el){return el.$id === key;});
    if(indexOfOption > -1){
      $scope.option = Options[indexOfOption];
    }
    var mappedOptions = [];
    for(var key in $scope.option.options){
      var indexOfOption = Options.findIndex(function(el){return el.$id === key;})
      if(indexOfOption > -1){
        mappedOptions.push(Options[indexOfOption])
      }
    }
    $scope.option.mappedOptions = mappedOptions;
  }
  Options.$watch(watchHandler);


  $scope.viewPrevious = function () {
    if ($scope.previousOption && $scope.previousOption.option_id){
      $state.go('option', {
        story_id: $scope.story.story_id,
        option_id: $scope.previousOption.option_id
      });  
    } else {
      $state.go('story', {
        story_id: $scope.story.story_id
      });
    }
  };
})

.controller('StoryCtrl', function($scope, $stateParams, Stories, Options) {
  var scope = $scope;
  var key = $stateParams.story_id;
  var watchHandler = function(valueChanged){
    var key = $stateParams.story_id;
    $scope.story = $scope.stories[key];
    if($scope.story && $scope.story.options && Options.length > 0){
      var mappedOptions = [];
      for(var key in $scope.story.options){
        var indexOfOption = Options.findIndex(function(el){return el.$id === key;})
        if(indexOfOption > -1){
          mappedOptions.push(Options[indexOfOption])
        }
      }
      $scope.story.mappedOptions = mappedOptions;
    }
  };
  $scope.story = $scope.stories[key];
  if($scope.story){
    var mappedOptions = [];
    for(var key in $scope.story.options){
      if(Options[key]){
        mappedOptions.push(Options[key])
      }
    }
    $scope.story.options = mappedOptions;
  }
  $scope.id = key;
  $scope.story = $scope.stories[key];
  $scope.id = key;
  Stories.$watch(watchHandler);
  Options.$watch(watchHandler);
})


.controller('AppCtrl', function($scope, $ionicHistory, Auth, Messages, $http, Stories) {
  $scope.myGoBack = function() {
    console.log('hit go back');
    $ionicHistory.goBack();
  };
  // EMAIL & PASSWORD AUTHENTICATION
  // Check for the user's authentication state
  Auth.$onAuth(function(authData) {
    if (authData) {
      $scope.loggedInUser = authData;
    } else {
      $scope.loggedInUser = null;
    }
  });

  // Create a new user, called when a user submits the signup form
  $scope.createUser = function(user) {
    Auth.$createUser({
      email: user.email,
      password: user.pass
    }).then(function() {
      // User created successfully, log them in
      return Auth.$authWithPassword({
        email: user.email,
        password: user.pass
      });
    }).then(function(authData) {
      console.log('Logged in successfully as: ', authData.uid);
      $scope.loggedInUser = authData;
    }).catch(function(error) {
      console.log(error);
      alert(error);
    });
  };

  // Login an existing user, called when a user submits the login form
  $scope.login = function(user) {
    Auth.$authWithPassword({
      email: user.email,
      password: user.pass
    }).then(function(authData) {
      console.log('Logged in successfully as: ', authData.uid);
      $scope.loggedInUser = authData;
    }).catch(function(error) {
      console.log(error);
      alert(error);
    });
  };

  $scope.getResults = function(queryString, cb) {
    //base64 encode the AppId
    var azureKey = btoa(':amltunEURSCruX/q8/GycMaIuwBKN5UsxeBkspnphHk');
    //get the value from the search box
    //Create the search string
    var myUrl = 'https://api.datamarket.azure.com/Bing/Search/v1/Image?Query=%27' + queryString +  '%27&Adult=%27Strict%27&$format=json';
    //Make post request to bing

    var req = {
      method: 'POST',
      url: myUrl,
      headers: {
        'Authorization': 'Basic ' + azureKey
      }
    }

    $http(req).then(
      //success ? 
      function(data){
        cb(null, data.data.d.results);
      },
      // failure? 
      function(data){
        cb("error");
      }); 
  };

  // Log a user out
  $scope.logout = function() {
    Auth.$unauth();
  };

  // ADD MESSAGES TO A SYNCHRONIZED ARRAY

  // Bind messages to the scope
  $scope.messages = Messages;
  // Bind stories to the scope
  $scope.stories = Stories;

  // Add a message to a synchronized array using $add with $firebaseArray
  $scope.addOption = function(message) {
    if ($scope.loggedInUser) {
      Messages.$add({
        email: $scope.loggedInUser.password.email,
        text: message.text
      });
      message.text = "";
    }
  };

})

.run(function($ionicPlatform, FBURL) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (FBURL === "https://YOUR-FIREABASE-APP.firebaseio.com/") {
      angular.element(document.getElementById('app-content')).html('<h1>Please configure your Firebase URL in www/js/app.js before running!</h1>');
    }
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});
